'use client';

import { useCallback, useEffect, useState } from "react";

import { analyzeGenesApi, getAllGenesApi, searchGenesApi } from "@/api/genes";
import { Card, CardContent, CardHeader, CardTitle } from "@/components/ui/card";
import { Input } from "@/components/ui/input";
import { Button } from "@/components/ui/button";
import { Table, TableBody, TableCell, TableHead, TableHeader, TableRow } from "@/components/ui/table";

export default function Page(): JSX.Element {
  const [genes, setGenes] = useState<any[]>([]);
  const [search, setSearch] = useState<string>('');
  const [searchResults, setSearchResults] = useState<any[]>([]);

  const searchGenes = useCallback(() => {
    const geneIDs = search.split(',');
    analyzeGenesApi(geneIDs).then(data => setSearchResults(data));
  }, [search]);

  const fetchData = () => {
    getAllGenesApi().then(data => setGenes(data));
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="h-full w-full flex items-center justify-center">
      <Card className="min-w-[450px]">
        <CardHeader>
          <CardTitle>Analyze Genes</CardTitle>
        </CardHeader>
        <CardContent>
          <Input
            placeholder="Search for a gene"
            value={search}
            onChange={(e) => setSearch(e.target.value)}
          />

          <Button
            className="mt-4 w-full"
            onClick={searchGenes}
          >
            Analyze
          </Button>

          {searchResults.length > 0 && (
            <Table className="mt-6">
              <TableHeader>
                <TableRow>
                  <TableHead>Gene</TableHead>
                  <TableHead>Transcript</TableHead>
                  <TableHead>Mean</TableHead>
                  <TableHead>Median</TableHead>
                  <TableHead>Variance</TableHead>
                </TableRow>
              </TableHeader>
              <TableBody>
                {searchResults.map((gene) => (
                  <TableRow key={gene.gene}>
                    <TableCell>{gene.gene}</TableCell>
                    <TableCell>{gene.transcript}</TableCell>
                    <TableCell>{gene.stats.mean}</TableCell>
                    <TableCell>{gene.stats.median}</TableCell>
                    <TableCell>{gene.stats.variance}</TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          )}
        </CardContent>
      </Card>
    </div>    
  );
}
