
const BASE_URL = 'http://localhost:3001';

export function getAllGenesApi() {
  return fetch(`${BASE_URL}/genes`).then(response => response.json());
}

export function searchGenesApi(geneIDs: string[]) {
  return fetch(`${BASE_URL}/genes/search?geneIDs=${geneIDs}`).then(response => response.json());
}

export function analyzeGenesApi(geneIDs: string[]) {
  return fetch(`${BASE_URL}/genes/stats?geneIDs=${geneIDs}`).then(response => response.json());
}