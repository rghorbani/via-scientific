'use client';

import Link from "next/link";

import { NavigationMenu, NavigationMenuItem, NavigationMenuLink } from "@/components/ui/navigation-menu";

export function Header() {
  return (
    <header className="flex justify-center p-4">
      <NavigationMenu className="list-none space-x-5">
        <NavigationMenuItem>
          <Link href="/">
            <NavigationMenuLink>
              Home
            </NavigationMenuLink>
          </Link>
        </NavigationMenuItem>

        <NavigationMenuItem>
          <Link href="/analyze">
            <NavigationMenuLink>
              Analyze
            </NavigationMenuLink>
          </Link>
        </NavigationMenuItem>
      </NavigationMenu>
    </header>
  )
}