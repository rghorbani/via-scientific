import { Module } from '@nestjs/common';

import { GeneModule } from './genes';

@Module({
  imports: [GeneModule],
})
export class AppModule {}
