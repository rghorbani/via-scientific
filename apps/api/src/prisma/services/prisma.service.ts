import {
  Injectable,
  OnModuleInit,
  OnModuleDestroy,
} from '@nestjs/common';
import { PrismaClient } from '../../../node_modules/.prisma/client/index.js';

@Injectable()
export class PrismaService
  extends PrismaClient
  implements OnModuleInit, OnModuleDestroy
{
  async onModuleInit() {
    await this.$connect();

    this.$use((params, next) => {
      if (params.action === 'delete') {
        params.action = 'update';
        params.args['data'] = { deleted_at: new Date() };
      }
      if (params.action == 'deleteMany') {
        params.action = 'updateMany';
        if (params.args.data != undefined) {
          params.args.data['deleted_at'] = new Date();
        } else {
          params.args['data'] = { deleted_at: new Date() };
        }
      }
      if (
        // params.action === 'findUnique' ||
        params.action === 'findFirst' ||
        params.action === 'findMany' ||
        params.action === 'findRaw'
      ) {
        if (params.args['paranoid'] == true) {
          params.args['where'] = {
            deleted_at: undefined,
            ...params.args['where'],
          };
        } else {
          params.args['where'] = {
            deleted_at: null,
            ...params.args['where'],
          };
        }
      }
      return next(params);
    });
  }

  async onModuleDestroy() {
    await this.$disconnect();
  }
}
