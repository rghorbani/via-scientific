import { Controller, Get, Post, Query } from '@nestjs/common';
import { Gene } from '@prisma/client';

import { GeneService } from '../services';

@Controller('genes')
export class GenesController {
  constructor(
    private readonly geneService: GeneService,
  ) {}

  @Get()
  genes(): Promise<Gene[]> {
    return this.geneService.getAllGenes();
  }

  @Get('search')
  searchGense(
    @Query('geneIDs') geneIDs: string
  ): Promise<Gene[]> {
    const geneIDsArray = geneIDs.split(',');
    return this.geneService.searchGenes(geneIDsArray);
  }

  @Get('stats')
  geneStats(
    @Query('geneIDs') geneIDs: string
  ): Promise<any[]> {
    const geneIDsArray = geneIDs.split(',');
    return this.geneService.analyzeGenes(geneIDsArray);
  }

  // @Post('anomolies')
  // geneAnomolies(): any[] {
  //   return this.geneService.getGenes([]);
  // }

  @Get('import')
  import(): any {
    return this.geneService.addDataFromSource();
  }
}
