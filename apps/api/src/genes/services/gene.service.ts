import { Injectable } from '@nestjs/common';
import { Gene } from '@prisma/client';
import * as csv from 'csv-parser';
import axios from 'axios';
import { mean, median, variance } from 'mathjs';

import { PrismaService } from '../../prisma';

@Injectable()
export class GeneService {
  constructor(
    private readonly prisma: PrismaService,
  ) {}

  getAllGenes(): Promise<Gene[]> {
    return this.prisma.gene.findMany({});
  }

  searchGenes(geneIDs: string[]): Promise<Gene[]> {
    return this.prisma.gene.findMany({
      where: {
        gene: { in: geneIDs },
      },
    });
  }

  async analyzeGenes(geneIDs: string[]): Promise<any[]> {
    const genes = await this.prisma.gene.findMany({
      where: {
        gene: { in: geneIDs },
      },
    });

    const results = [];
    for (const gene of genes) {
      const values = [
        gene.exper_rep1,
        gene.exper_rep2,
        gene.exper_rep3,
        gene.control_rep1,
        gene.control_rep2,
        gene.control_rep3,
      ];

      const stats = {
        mean: mean(values),
        median: median(values),
        variance: variance(values),
      };

      results.push({
        gene: gene.gene,
        transcript: gene.transcript,
        stats,
      });
    }

    return results;
  }

  async addDataFromSource(): Promise<any> {
    const url = 'https://umms.dolphinnext.com/pub/debrowser/simple_demo.tsv';
    const response = await axios.get(url, { responseType: 'stream' });
    const results = [];

    response.data.pipe(csv({ separator: '\t' }))
      .on('data', (data) => results.push(data))
      .on('end', async () => {
        for (const item of results) {
          await this.prisma.gene.create({
            data: {
              gene: item.gene,
              transcript: item.transcript,
              exper_rep1: parseFloat(item.exper_rep1),
              exper_rep2: parseFloat(item.exper_rep2),
              exper_rep3: parseFloat(item.exper_rep3),
              control_rep1: parseFloat(item.control_rep1),
              control_rep2: parseFloat(item.control_rep2),
              control_rep3: parseFloat(item.control_rep3),
            },
          });
        }
      });

    return 'Done';
  }
}
