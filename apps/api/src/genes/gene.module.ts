import { Module } from '@nestjs/common';

import { PrismaModule } from '../prisma';
import { GenesController } from './controllers';
import { GeneService } from './services';

@Module({
  controllers: [GenesController],
  imports: [
    PrismaModule,
  ],
  providers: [GeneService],
})
export class GeneModule {}
