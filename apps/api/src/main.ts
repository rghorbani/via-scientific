import { NestFactory } from '@nestjs/core';

import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.enableCors({ maxAge: 600 });

  app.enableShutdownHooks();

  const config: DocumentBuilder = new DocumentBuilder()
    .setTitle('App')
    .setDescription('App description')
    .setVersion('1.0')
    .addTag('app');
  const document = SwaggerModule.createDocument(app, config.build());
  SwaggerModule.setup('api-docs', app, document);

  await app.listen(3001);
}
bootstrap();
