-- CreateTable
CREATE TABLE "genes" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "gene" TEXT NOT NULL,
    "transcript" TEXT NOT NULL,
    "exper_rep1" REAL NOT NULL,
    "exper_rep2" REAL NOT NULL,
    "exper_rep3" REAL NOT NULL,
    "control_rep1" REAL NOT NULL,
    "control_rep2" REAL NOT NULL,
    "control_rep3" REAL NOT NULL,
    "created_at" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" DATETIME NOT NULL,
    "deleted_at" DATETIME
);
